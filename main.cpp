/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>

using namespace cimg_library;

//constant that defines how many times we will repeat the algorithm (to be coherent with single-thread program times)
const int loops=20;

// Data type for image components

typedef float data_t;

//data type that will hold all of the information of the images in the main function (height, width, modifiers of the algorithm...)
typedef struct{
	data_t* imgSrc;
	data_t* imgSrc2;
	data_t* imgDst;
	int width;
	int height;
	int startingRow;
	int nRows;
	float pr;
	float pg;
	float pb;
}ThreadParams;

const char* SOURCE_IMG      = "bailarina.bmp"; //first image
const char* DESTINATION_IMG = "bailarina2.bmp"; //new image generated
const char* SOURCE_IMG_2    = "flores_3.bmp"; //second image

//multithread function
void* ThreadProc(void* arg)
{
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of the first image (X)
	data_t *pRsrc2, *pGsrc2, *pBsrc2; // Pointers to the R, G and B components of the second image (Y)
	data_t *pRdest, *pGdest, *pBdest; // Pointers to the R, G and B components of the destination image

	ThreadParams args = *((ThreadParams*)arg);
	float Rts=args.pr;
	float Gts=args.pg;
	float Bts=args.pb;

	//destination image component array
	pRdest=args.imgDst+args.startingRow;  //pRcomp points to the R component array
	pGdest=pRdest+args.width*args.height; //pGcomp points to the G component array
	pBdest=pGdest+args.width*args.height; //pBcomp points to the B component array

	//source image 1 (X) image component array
	pRsrc=args.imgSrc+args.startingRow; //pRcomp points to the R component array
	pGsrc=pRsrc+args.width*args.height; //pGcomp points to the G component array
	pBsrc=pGsrc+args.width*args.height; //pBcomp points to the B component array

	//source image 2 (Y) image component array
	pRsrc2=args.imgSrc2+args.startingRow; //pRcomp points to the R component array
	pGsrc2=pRsrc2+args.width*args.height; //pGcomp points to the G component array
	pBsrc2=pGsrc2+args.width*args.height; //pBcomp points to the B component array

	//algorithm #8
	for (int i = 0; i < args.nRows; i++){
	//	*(pRdest + i) = *(pRsrc + i) * Rt;  // This is equals to pRdest[i] = pGsrc[i]
		pRdest[i]= (256 * (pRsrc2[i] * Rts)) / ((pRsrc[i] * Rts) + 1 );
		pGdest[i]= (256 * (pGsrc2[i] * Gts)) / ((pGsrc[i] * Gts) + 1 );
		pBdest[i]= (256 * (pBsrc2[i] * Bts)) / ((pBsrc[i] * Bts) + 1 );	
	}		
    return NULL;
}

int main() {
	//Multhithread 
	const int N = 4; //number of threads
    pthread_t thread[N]; //thread declaration
	// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> srcImage2(SOURCE_IMG_2);

	//test if the images exist
	if (srcImage==NULL){
		printf("First image does not exist");
		return 0;
	}
	if (srcImage2==NULL){
		printf("Second image does not exist");
		return 0;
	}

	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image. Both images must be equals in size and height
	uint nComp; // Number of image components

	struct timespec tStart, tEnd;
	double dElapsedTimeS;
	float Rt, Gt, Bt;
	//modifiers of the algorythm
	Rt = 0.4; //red modifier
	Gt = 0.1; //green modifier
	Bt = 0.2; //blue modifier

	if (srcImage.width()!=srcImage2.width() || srcImage.height()!=srcImage2.height()){
		perror("images are not equal size");
		exit (-2);
	}

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	srcImage2.display(); // Displays the source image

	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	//start measuring time
	if (clock_gettime(CLOCK_REALTIME, &tStart)<0){
		perror("error while starting time measure");
		exit (-2);
	}

//algorithm needs to be repeated loops times, to be coherent with the single-thread program (to measure times)
for (int j=0;j<loops;j++){
	int numPix=(width*height)/N; //this will divide the rows of the image to process based on the number of threads we specify on N
	ThreadParams params[N];
	for (int i=0; i<N;i++){
		params[i].height=height; //image size data (pixels)
		params[i].width=width;
		params[i].imgSrc=srcImage.data(); //image data
		params[i].imgSrc2=srcImage2.data();
		params[i].imgDst=pDstImage;
		params[i].nRows=numPix; //number of rows that every thread will work on
		params[i].startingRow= i*numPix; //every thread will do the algorithm on a specified row, this specifies what will be the starting row
		params[i].pr=Rt; //modifiers of our algorithm
		params[i].pg=Gt;
		params[i].pb=Bt;
		if (pthread_create(&thread[i], NULL, ThreadProc, &(params[i]) )!= 0){ //create individual thread
            fprintf(stderr, "ERROR: Creating thread %d\n", i);
            return EXIT_FAILURE;
        }
	}

    // Wait till the completion of all threads
    printf("Main thread waiting...\n");
    for (int i = 0; i < N; i++){
        pthread_join(thread[i], NULL);
    }
    printf("Main thread finished.\n");
}

	if (clock_gettime(CLOCK_REALTIME, &tEnd)<0){ //stop measuring time
		perror("error at the end of time measuring");
		exit (-2);
	}
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) /1e+9;
		
	printf("\n\n\nEste es el tiempo %f\n\n\n", dElapsedTimeS);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.

	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	dstImage.cut(0,255); //this makes all pixels of the image to be between values 0 and 255 (if they are lower, values are set to 0 and higher to 255)
	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);
	return 0;
}



